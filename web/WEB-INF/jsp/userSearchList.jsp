<%--
    Document   : userSearchList
    Created on : Apr 5, 2018, 7:27:33 PM
    Author     : Madou
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div class="">
            <c:forEach items="${users}" var="user">
                <!-- Example Social Card-->
                <div class="card mb-3">
                    <div class="card-body">
                        <a href="profile?id=${user.getUser_ID()}&pub=1"><h5 class="card-title mb-1"><c:out value="${user.getUser_Firstname()}" /> <c:out value="${user.getUser_Lastname()}" /></h5></a>
                        <p class="card-text small"></p>
                    </div>
                    <hr class="my-0">
                </div>
            </c:forEach>
        </div>
    </body>
</html>
