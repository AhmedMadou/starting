<html>

<head>
  <meta charset="utf-8">
  <title>Log-In</title>

  <link rel="stylesheet" href="../../ressources/css/login_style.css">
  <link rel="stylesheet" href="ressources/css/core.css">
</head>

<body>

  
  <div class="cotn_principal" style="padding-top:40px">
      
    <div class="cont_centrar">
        <h1>LOGIN</h1>
      <div class="cont_login">
        <div class="cont_info_log_sign_up">
          <div class="col_md_login">
            <div class="cont_ba_opcitiy">

              <h2>RegAdmin</h2>
              <p>Connectez-vous pour voter et savoir des informations sur les condidats </p>
              <button class="btn_login" onclick="cambiar_login()">Login</button>
            </div>
          </div>
          <div class="col_md_sign_up">
          </div>
        </div>


        <div class="cont_back_info" style="margin-bottom:150px;">
          <div class="cont_img_back_grey">
            <img src="ressources/images/bg/slider_2.jpg"
              alt="" />
          </div>

        </div>
        <div class="cont_forms">
          <div class="cont_img_back_">
            <img src="ressources/images/bg/slider_2.jpg"
              alt="" />
          </div>
          <div class="cont_form_login">
            <a href="#" onclick="ocultar_login_sign_up()">
              <i class="fa fa-arrow-left"></i>
            </a>
            <h2>RegAdmin</h2>
            <form>
				<input type="email" placeholder="email" />
				<input type="text" placeholder="CIN" />
				<input type="text" placeholder="Nom" />
				<button class="btn_login" onclick="cambiar_login()">Login</button>
            </form>
          </div>

          <div class="cont_form_sign_up">
          </div>

        </div>
        
        <a href="signup.html" style="text-align: center; margin-top:100px; font-size:20px; color: black">Not having an account?</a>
      </div>
    </div>
  </div>
  <script>

    function cambiar_login() {
      document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";
      document.querySelector('.cont_form_login').style.display = "block";
      document.querySelector('.cont_form_sign_up').style.opacity = "0";

      setTimeout(function () { document.querySelector('.cont_form_login').style.opacity = "1"; }, 400);

      setTimeout(function () {
        document.querySelector('.cont_form_sign_up').style.display = "none";
      }, 200);
    }

    function cambiar_sign_up(at) {
      document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
      document.querySelector('.cont_form_sign_up').style.display = "block";
      document.querySelector('.cont_form_login').style.opacity = "0";

      setTimeout(function () {
        document.querySelector('.cont_form_sign_up').style.opacity = "1";
      }, 100);

      setTimeout(function () {
        document.querySelector('.cont_form_login').style.display = "none";
      }, 400);


    }



    function ocultar_login_sign_up() {

      document.querySelector('.cont_forms').className = "cont_forms";
      document.querySelector('.cont_form_sign_up').style.opacity = "0";
      document.querySelector('.cont_form_login').style.opacity = "0";

      setTimeout(function () {
        document.querySelector('.cont_form_sign_up').style.display = "none";
        document.querySelector('.cont_form_login').style.display = "none";
      }, 500);

    }




  </script>
</body>

</html>
