<%-- 
    Document   : AffichageVote
    Created on : 7 avr. 2018, 20:34:36
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>VoteHere</h1>
        
        <form:form method="POST" action="vote" modelAttribute="party">
            Numero: <form:input path="party_nvote" type="number" />
            <button type="submit">ok</button>
        </form:form>
    </body>
</html>
