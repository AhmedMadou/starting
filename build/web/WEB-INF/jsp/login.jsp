<%--
    Document   : Login
    Created on : Apr 5, 2018, 3:06:34 PM
    Author     : Madou
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Log-In</title>
        <style>
            * {
                margin: 0px auto;
                padding: 0px;
                text-align: center;
                font-family: 'Open Sans', sans-serif;
            }

            .cotn_principal {
                position: absolute;
                width: 100%;
                height: 100%;
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#cfd8dc+0,607d8b+100,b0bec5+100 */
                background: #cfd8dc;
                /* Old browsers */
                background: -moz-linear-gradient(-45deg, #cfd8dc 0%, #607d8b 100%, #b0bec5 100%);
                /* FF3.6-15 */
                background: -webkit-linear-gradient(-45deg, #cfd8dc 0%, #607d8b 100%, #b0bec5 100%);
                /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(135deg, #cfd8dc 0%, #607d8b 100%, #b0bec5 100%);
                /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cfd8dc', endColorstr='#b0bec5', GradientType=1);
                /* IE6-9 fallback on horizontal gradient */
            }


            .cont_centrar {
                position: relative;
                float: left;
                width: 100%;
            }

            .cont_login {
                position: relative;
                width: 640px;
                left: 50%;
                margin-left: -320px;

            }

            .cont_back_info {
                position: relative;
                float: left;
                width: 640px;
                height: 280px;
                overflow: hidden;
                background-color: #fff;
                margin-top: 100px;
                box-shadow: 1px 10px 30px -10px rgba(0, 0, 0, 0.5);
            }

            .cont_forms {
                position: absolute;
                overflow: hidden;
                top: 100px;
                left: 0px;
                width: 320px;
                height: 280px;
                background-color: #eee;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }

            .cont_forms_active_login {
                box-shadow: 1px 10px 30px -10px rgba(0, 0, 0, 0.5);
                height: 420px;
                top: 20px;
                left: 0px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;

            }

            .cont_forms_active_sign_up {
                box-shadow: 1px 10px 30px -10px rgba(0, 0, 0, 0.5);
                height: 420px;
                top: 20px;
                left: 320px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }

            .cont_img_back_grey {
                position: absolute;
                width: 950px;
                top: -80px;
                left: -116px;
            }

            .cont_img_back_grey>img {
                width: 100%;
                -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
                opacity: 0.2;
                animation-name: animar_fondo;
                animation-duration: 20s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
                animation-direction: alternate;

            }

            .cont_img_back_ {
                position: absolute;
                width: 950px;
                top: -80px;
                left: -116px;
            }

            .cont_img_back_>img {
                width: 100%;
                opacity: 0.3;
                animation-name: animar_fondo;
                animation-duration: 20s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
                animation-direction: alternate;
            }

            .cont_forms_active_login>.cont_img_back_ {
                top: 0px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }

            .cont_forms_active_sign_up>.cont_img_back_ {
                top: 0px;
                left: -435px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }


            .cont_info_log_sign_up {
                position: absolute;
                width: 640px;
                height: 280px;
                top: 100px;
                z-index: 1;
            }

            .col_md_login {
                position: relative;
                float: left;
                width: 50%;
            }

            .col_md_login>h2 {
                font-weight: 400;
                margin-top: 70px;
                color: #757575;
            }

            .col_md_login>p {
                font-weight: 400;
                margin-top: 15px;
                width: 80%;
                color: #37474F;
            }

            .btn_login {
                background-color: #26C6DA;
                border: none;
                padding: 10px;
                width: 200px;
                border-radius: 3px;
                box-shadow: 1px 5px 20px -5px rgba(0, 0, 0, 0.4);
                color: #fff;
                margin-top: 10px;
                cursor: pointer;
            }

            .col_md_sign_up {
                position: relative;
                float: left;
                width: 50%;
            }

            .cont_ba_opcitiy>h2 {
                font-weight: 400;
                color: #fff;
            }

            .cont_ba_opcitiy>p {
                font-weight: 400;
                margin-top: 15px;
                color: #fff;
            }

            .cont_ba_opcitiy {
                position: relative;
                background-color: rgba(120, 144, 156, 0.55);
                width: 80%;
                border-radius: 3px;
                margin-top: 60px;
                padding: 15px 0px;
            }

            .btn_sign_up {
                background-color: #ef5350;
                border: none;
                padding: 10px;
                width: 200px;
                border-radius: 3px;
                box-shadow: 1px 5px 20px -5px rgba(0, 0, 0, 0.4);
                color: #fff;
                margin-top: 10px;
                cursor: pointer;
            }

            .cont_forms_active_sign_up {
                z-index: 2;
            }


            @-webkit-keyframes animar_fondo {
                from {
                    -webkit-transform: scale(1) translate(0px);
                    -moz-transform: scale(1) translate(0px);
                    -ms-transform: scale(1) translate(0px);
                    -o-transform: scale(1) translate(0px);
                    transform: scale(1) translate(0px);
                }
                to {
                    -webkit-transform: scale(1.5) translate(50px);
                    -moz-transform: scale(1.5) translate(50px);
                    -ms-transform: scale(1.5) translate(50px);
                    -o-transform: scale(1.5) translate(50px);
                    transform: scale(1.5) translate(50px);
                }
            }

            @-o-keyframes identifier {
                from {
                    -webkit-transform: scale(1);
                    -moz-transform: scale(1);
                    -ms-transform: scale(1);
                    -o-transform: scale(1);
                    transform: scale(1);
                }
                to {
                    -webkit-transform: scale(1.5);
                    -moz-transform: scale(1.5);
                    -ms-transform: scale(1.5);
                    -o-transform: scale(1.5);
                    transform: scale(1.5);
                }

            }

            @-moz-keyframes identifier {
                from {
                    -webkit-transform: scale(1);
                    -moz-transform: scale(1);
                    -ms-transform: scale(1);
                    -o-transform: scale(1);
                    transform: scale(1);
                }
                to {
                    -webkit-transform: scale(1.5);
                    -moz-transform: scale(1.5);
                    -ms-transform: scale(1.5);
                    -o-transform: scale(1.5);
                    transform: scale(1.5);
                }

            }

            @keyframes identifier {
                from {
                    -webkit-transform: scale(1);
                    -moz-transform: scale(1);
                    -ms-transform: scale(1);
                    -o-transform: scale(1);
                    transform: scale(1);
                }
                to {
                    -webkit-transform: scale(1.5);
                    -moz-transform: scale(1.5);
                    -ms-transform: scale(1.5);
                    -o-transform: scale(1.5);
                    transform: scale(1.5);
                }
            }

            .cont_form_login {
                position: absolute;
                opacity: 0;
                display: none;
                width: 320px;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }

            .cont_forms_active_login {
                z-index: 2;
            }

            .cont_forms_active_login>.cont_form_login {}

            .cont_form_sign_up {
                position: absolute;
                width: 320px;
                float: left;
                opacity: 0;
                display: none;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }


            .cont_form_sign_up>input {
                text-align: left;
                padding: 15px 5px;
                margin-left: 10px;
                margin-top: 20px;
                width: 260px;
                border: none;
                color: #757575;
            }

            .cont_form_sign_up>h2 {
                margin-top: 50px;
                font-weight: 400;
                color: #757575;
            }


            .cont_form_login>input {
                padding: 15px 5px;
                margin-left: 10px;
                margin-top: 20px;
                width: 260px;
                border: none;
                text-align: left;
                color: #757575;
            }

            .cont_form_login>h2 {
                margin-top: 110px;
                font-weight: 400;
                color: #757575;
            }

            .cont_form_login>a,
            .cont_form_sign_up>a {
                color: #757575;
                position: relative;
                float: left;
                margin: 10px;
                margin-left: 30px;
            }
        </style>

        <link rel="stylesheet" href="./Log-In_files/core.css">
    </head>

    <body>


        <div class="cotn_principal" style="padding-top:40px">

            <div class="cont_centrar">
                <h1>LOGIN</h1>
                <div class="cont_login">
                    <div class="cont_info_log_sign_up">
                        <div class="col_md_login">
                            <div class="cont_ba_opcitiy">

                                <h2>�lecteur</h2>
                                <p>Connectez-vous pour voter et savoir des informations sur les condidats </p>
                                <button class="btn_login" onclick="cambiar_login()">Login</button>
                            </div>
                        </div>
                        <div class="col_md_sign_up">
                            <div class="cont_ba_opcitiy">
                                <h2>Liste �lection</h2>
                                <p>Se connecter au compte de la partie politique</p>

                                <button class="btn_sign_up" onclick="cambiar_sign_up()">Login</button>
                            </div>
                        </div>
                    </div>


                    <div class="cont_back_info" style="margin-bottom:150px;">
                        <div class="cont_img_back_grey">
                            <img src="./Log-In_files/slider_2.jpg" alt="">
                        </div>

                    </div>
                    <div class="cont_forms">
                        <div class="cont_img_back_">
                            <img src="./Log-In_files/slider_2.jpg" alt="">
                        </div>
                        <div class="cont_form_login">
                            <a href="http://192.168.8.101:8000/shared/login%20%28copy%29.html#" onclick="ocultar_login_sign_up()">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <h2>�lecteur</h2>
                            <form id="userLoginForm" modelAttribute="login" action="userLoginProcess" method="post">
                                <input type="text" placeholder="Email">
                                <input type="password" placeholder="Password">
                                <button class="btn_login" onclick="cambiar_login()">Login</button>
                            </form>
                        </div>

                        <div class="cont_form_sign_up">
                            <a href="http://192.168.8.101:8000/shared/login%20%28copy%29.html#" onclick="ocultar_login_sign_up()">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <h2>Liste d'�lection</h2>
                            <form id="ListLoginForm" modelAttribute="login" action="listLoginProcess" method="post">
                                <input type="text" placeholder="Email">
                                <input type="password" placeholder="Password">
                                <button class="btn_sign_up" onclick="cambiar_sign_up()">Login</button>
                            </form>
                        </div>

                    </div>

                    <a href="http://192.168.8.101:8000/shared/signup.html" style="text-align: center; margin-top:100px; font-size:20px; color: black">Not having an account?</a>
                </div>
            </div>
        </div>
        <script>

            function cambiar_login() {
                document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";
                document.querySelector('.cont_form_login').style.display = "block";
                document.querySelector('.cont_form_sign_up').style.opacity = "0";

                setTimeout(function () {
                    document.querySelector('.cont_form_login').style.opacity = "1";
                }, 400);

                setTimeout(function () {
                    document.querySelector('.cont_form_sign_up').style.display = "none";
                }, 200);
            }

            function cambiar_sign_up(at) {
                document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
                document.querySelector('.cont_form_sign_up').style.display = "block";
                document.querySelector('.cont_form_login').style.opacity = "0";

                setTimeout(function () {
                    document.querySelector('.cont_form_sign_up').style.opacity = "1";
                }, 100);

                setTimeout(function () {
                    document.querySelector('.cont_form_login').style.display = "none";
                }, 400);


            }



            function ocultar_login_sign_up() {

                document.querySelector('.cont_forms').className = "cont_forms";
                document.querySelector('.cont_form_sign_up').style.opacity = "0";
                document.querySelector('.cont_form_login').style.opacity = "0";

                setTimeout(function () {
                    document.querySelector('.cont_form_sign_up').style.display = "none";
                    document.querySelector('.cont_form_login').style.display = "none";
                }, 500);

            }




        </script>



        <span style="border-radius: 3px; text-indent: 20px;f width: auto; padding: 0px 4px 0px 0px; text-align: center; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 11px; line-height: 20px; font-family: "Helvetica Neue", Helvetica, sans-serif; color: rgb(255, 255, 255); background: url(&quot;data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjYmQwODFjIj48L3BhdGg+PC9nPjwvc3ZnPg==&quot;) 3px 50% / 14px 14px no-repeat rgb(189, 8, 28); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; -webkit-font-smoothing: antialiased;">Save</span><span style="width: 24px; height: 24px; background: url(&quot;data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pjxzdmcgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDI0IDI0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxtYXNrIGlkPSJtIj48cmVjdCBmaWxsPSIjZmZmIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHJ4PSI2IiByeT0iNiIvPjxyZWN0IGZpbGw9IiMwMDAiIHg9IjUiIHk9IjUiIHdpZHRoPSIxNCIgaGVpZ2h0PSIxNCIgcng9IjEiIHJ5PSIxIi8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMTAiIHk9IjAiIHdpZHRoPSI0IiBoZWlnaHQ9IjI0Ii8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMCIgeT0iMTAiIHdpZHRoPSIyNCIgaGVpZ2h0PSI0Ii8+PC9tYXNrPjwvZGVmcz48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IiNmZmYiIG1hc2s9InVybCgjbSkiLz48L3N2Zz4=&quot;) 50% 50% / 14px 14px no-repeat rgba(0, 0, 0, 0.4); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; border-radius: 12px;"></span></body></html>