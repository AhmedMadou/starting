<%--
    Document   : userWelcomePage
    Created on : Apr 5, 2018, 5:38:24 PM
    Author     : Madou
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>this is user Welcome Page</h1>
        <br>
        <!-- this is a form for search -->
        <form action="searchUser" method="GET" class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
                <input  name="name" class="form-control" type="text" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search">Look For</i>
                    </button>
                </span>
            </div>
        </form>

        <br>
        <a href="logout">logout</a>
    </body>
</html>
