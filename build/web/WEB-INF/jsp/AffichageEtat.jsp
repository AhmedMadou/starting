<%-- 
    Document   : AffichageEtat
    Created on : 7 avr. 2018, 17:40:59
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste des gouvernorat en Tunisie</h1>
       <table>
        <c:forEach items="${listeEtat}" var="etat" >    
        <tr>
            <td>
                
                <a href="EtatController?link_codeEtat=${etat.etat_nom}"> <c:out value="${etat.etat_nom} "/> </a>
                
            </td>
            
       </tr>
    
        </c:forEach>
         </table>
    </body>
</html>
