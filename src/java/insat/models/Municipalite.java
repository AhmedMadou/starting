/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import insat.dao.MunicipaliteDao;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author asus
 */
@Entity
@Table(name = "MUNICIPALITE")
public class Municipalite implements Serializable {
    
    @Id
    @Column(name = "MUNICIPALITE_NUMERO")
    private int municipalite_numero;
    
    
    @Column(name = "MUNICIPALITE_NOM")
    private String municipalite_nom;
    
    
    @Column(name="MUNICIPALITE_etat")
    private String municipalite_etat;
    
    @Transient
    private MunicipaliteDao municipaliteDao=new MunicipaliteDao();

    public int getMunicipalite_numero() {
        return municipalite_numero;
    }

    public String getMunicipalite_nom() {
        return municipalite_nom;
    }

   
    public void setMunicipalite_nom(String municipalite_nom) {
        this.municipalite_nom = municipalite_nom;
    }

    

   

    

    
    

    public void setMunicipaliteDao(MunicipaliteDao municipaliteDao) {
        this.municipaliteDao = municipaliteDao;
    }

    public MunicipaliteDao getMunicipaliteDao() {
        return municipaliteDao;
    }

    public void setMunicipalite_numero(int municipalite_numero) {
        this.municipalite_numero = municipalite_numero;
    }

   public List<Municipalite> listeMunicipaliteParEtat(String etat){
         return municipaliteDao.listeMunicipaliteParEtat(etat);
     }

    public String getMunicipalite_etat() {
        return municipalite_etat;
    }

    
    
}
