/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import insat.dao.EtatDao;
import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author asus
 */
@Entity
@Table(name = "ETAT")
public class Etat implements Serializable {
    
    @Id
    @Column(name = "ETAT_NUMERO")
    private int etat_numero;
    
    
    @Column(name = "ETAT_NOM")
    private String etat_nom;
    
    
    @Column(name = "ETAT_POSITION")
    private int etat_position;
    
    @Transient
    private EtatDao etatDao=new EtatDao();
    
    

    public void setEtat_position(int etat_position) {
        this.etat_position = etat_position;
    }
    
    

    

    public int getEtat_position() {
        return etat_position;
    }

    
    
    
    public int getEtat_numero() {
        return etat_numero;
    }

    public String getEtat_nom() {
        return etat_nom;
    }

    public void setEtat_numero(int etat_numero) {
        this.etat_numero = etat_numero;
    }

    public void setEtat_nom(String etat_nom) {
        this.etat_nom = etat_nom;
    }
    
    public ArrayList<Etat> listeDesEtats(){
        return etatDao.listeDesEtats();
    }
    
}
