/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import insat.dao.EtatDao;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Madou
 */
@Entity
@Table(name = "USERS")
public class User implements Serializable {

    @Id
    @Column(name = "USER_CIN")
    @NotNull
    private int user_cin;

    @Column(name = "USER_PASSWORD")
    private String user_password;

    @Column(name = "USER_NOM")
    private String user_nom;

    @Column(name = "USER_VOTE")
    private boolean user_vote;

    public EtatDao getEtatDao() {
        return etatDao;
    }

    public void setEtatDao(EtatDao etatDao) {
        this.etatDao = etatDao;
    }

    @Transient
    private EtatDao etatDao = new EtatDao();

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public void setUser_cin(int user_cin) {
        this.user_cin = user_cin;
    }

    public void setUser_nom(String user_nom) {
        this.user_nom = user_nom;
    }

    public void setUser_vote(boolean user_vote) {
        this.user_vote = user_vote;
    }

    public int getUser_cin() {
        return user_cin;
    }

    public String getUser_nom() {
        return user_nom;
    }

    public boolean isUser_vote() {
        return user_vote;
    }

}
