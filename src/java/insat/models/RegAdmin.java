/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Madou
 */
@Entity
@Table(name = "REGADMIN")
public class RegAdmin implements Serializable {

    @Id
    @Column(name = "REGADMIN_CIN")
    private long regAdmin_ID;

    @Column(name = "REGADMIN_PASSWORD")
    private String regAdmin_Password;

    public RegAdmin() {
    }

    public RegAdmin(long regAdmin_ID, String regAdmin_Password) {
        this.regAdmin_ID = regAdmin_ID;
        this.regAdmin_Password = regAdmin_Password;
    }

    public long getRegAdmin_ID() {
        return regAdmin_ID;
    }

    public void setRegAdmin_ID(long regAdmin_ID) {
        this.regAdmin_ID = regAdmin_ID;
    }

    public String getRegAdmin_Password() {
        return regAdmin_Password;
    }

    public void setRegAdmin_Password(String regAdmin_Password) {
        this.regAdmin_Password = regAdmin_Password;
    }

}
