/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import insat.dao.PartyDao;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author asus
 */
@Entity
@Table(name = "PARTY")
public class Party implements Serializable {

    @Id
    @Column(name = "PARTY_NOM")
    private int party_nom;

    @Column(name = "PARTY_LOGIN")
    private int party_login;

    @Column(name = "PARTY_PASSWORD")
    private String party_password;

    @Column(name = "PARTY_DESCRIPTION")
    private String party_description;

    @Column(name = "PARTY_PROFILE_PIC")
    private String party_profile_pic;

    @Column(name = "PARTY_COVER_PIC")
    private String party_cover_pic;
    
    @Column (name= "PARTY_MUNICIPALITE")
    private String party_municipalite; 
    
    @Column(name="PARTY_NVOTE")
    private int party_nvote;

    public void setParty_municipalite(String party_municipalite) {
        this.party_municipalite = party_municipalite;
    }

    public void setParty_nvote(int party_nvote) {
        this.party_nvote = party_nvote;
    }

    public String getParty_municipalite() {
        return party_municipalite;
    }

    public int getParty_nvote() {
        return party_nvote;
    }
    
    
    
    public Party() {
    }

    public Party(int party_nom, int party_login, String party_password, String party_description, String party_profile_pic, String party_cover_pic) {
        this.party_nom = party_nom;
        this.party_login = party_login;
        this.party_password = party_password;
        this.party_description = party_description;
        this.party_profile_pic = party_profile_pic;
        this.party_cover_pic = party_cover_pic;
    }

    public int getParty_login() {
        return party_login;
    }

    public void setParty_login(int party_login) {
        this.party_login = party_login;
    }

    public String getParty_password() {
        return party_password;
    }

    public void setParty_password(String party_password) {
        this.party_password = party_password;
    }

    public String getParty_description() {
        return party_description;
    }

    public void setParty_description(String party_description) {
        this.party_description = party_description;
    }

    public String getParty_profile_pic() {
        return party_profile_pic;
    }

    public void setParty_profile_pic(String party_profile_pic) {
        this.party_profile_pic = party_profile_pic;
    }

    public String getParty_cover_pic() {
        return party_cover_pic;
    }

    public void setParty_cover_pic(String party_cover_pic) {
        this.party_cover_pic = party_cover_pic;
    }

    public int getParty_nom() {
        return party_nom;
    }

    public void setParty_nom(int party_nom) {
        this.party_nom = party_nom;
    }

    
}
