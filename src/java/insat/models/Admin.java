/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Madou
 */
@Entity
@Table(name = "ADMIN")
public class Admin implements Serializable {

    @Id
    @Column(name = "ADMIN_CIN")
    private long admin_ID;
    
    @Column(name = "ADMIN_PASSWORD")
    private String admin_Password;

    

    public long getAdmin_ID() {
        return admin_ID;
    }

    public void setAdmin_ID(long admin_ID) {
        this.admin_ID = admin_ID;
    }

    

    
    public String getAdmin_Password() {
        return admin_Password;
    }

    public void setAdmin_Password(String admin_Password) {
        this.admin_Password = admin_Password;
    }

}
