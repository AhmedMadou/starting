/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.services;

import insat.dao.PartyDaoImpl;
import insat.dao.UserDaoImpl;
import insat.models.Login;
import insat.models.Party;
import insat.models.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Madou
 */
@Service
public class PartyServiceImpl implements PartyService {

    @Autowired
    PartyDaoImpl partyDao;

    public void register(Party party) {
        partyDao.register(party);
    }

    public Party validateParty(Login login) {
        return partyDao.validateParty(login);
    }

    public List<Party> partySearch(String name) {
        return partyDao.partySearch(name);
    }

    public Party getParty(long id) {
        return partyDao.getParty(id);
    }

    @Override
    public List<Party> getPartys() {
        return partyDao.getPartys();
    }

    @Override
    public void deleteParty(long id) {
        partyDao.deleteParty(id);
    }

    
    public void update(Party party) {
       partyDao.updateParty(party);    }
}
