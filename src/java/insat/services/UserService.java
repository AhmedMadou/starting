/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.services;

import insat.models.Login;
import insat.models.User;
import java.util.List;

/**
 *
 * @author Madou
 */
public interface UserService {

    public void register(User user);

    public User validateUser(Login login);

    public List<User> userSearch(String name);

    public User getUser(long id);

    public List<User> getUsers();

    public void deleteUser(long id);

}
