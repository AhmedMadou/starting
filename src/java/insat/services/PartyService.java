/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.services;

import insat.models.Login;
import insat.models.Party;
import java.util.List;

/**
 *
 * @author Madou
 */
public interface PartyService {

    public void register(Party user);

    public Party validateParty(Login login);

    public List<Party> partySearch(String name);

    public Party getParty(long id);

    public List<Party> getPartys();

    public void deleteParty(long id);
    
    public void update(Party party);
}
