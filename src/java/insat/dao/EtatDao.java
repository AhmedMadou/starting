/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

import insat.models.Etat;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author asus
 */
public class EtatDao {
    
     public ArrayList<Etat> listeDesEtats(){
        Configuration con= new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Etat.class);
		
	SessionFactory sf= con.buildSessionFactory();
	Session session=sf.openSession();
	Transaction tx= session.beginTransaction();
        ArrayList<Etat> m =(ArrayList<Etat>) session.createCriteria(Etat.class).list();	
	tx.commit();
        return m;
    }
    
}
