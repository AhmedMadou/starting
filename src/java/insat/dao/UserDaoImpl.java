/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

import insat.models.Login;
import insat.models.User;
import java.util.List;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 *
 * @author Madou
 */
public class UserDaoImpl implements UserDao {

    public void register(User user) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();

        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            session.save(user);
            //commit the transaction
            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

    public User validateUser(Login login) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<User> user;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User as u where u.user_cin='" + login.getLogin() + "' and u.user_password='" + login.getPassword() + "'";
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (!user.isEmpty()) {
            return user.get(0);
        }
        return null;

    }

    public List<User> userSearch(String name) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<User> user;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User as u where lower(u.user_Firstname) like lower('%" + name + "%') or lower(u.user_Lastname) like lower('%" + name + "%') or u.user_Email='" + name + "' or str(u.user_Number)='" + name + "' or concat(u.user_Firstname, ' ', u.user_Lastname)='" + name + "' or concat(u.user_Lastname, ' ', u.user_Firstname)='" + name + "'";
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        return user;
    }

    public User getUser(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        User user = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            /*String query = "from User as u where u.user_ID=" + id;
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();*/

            user = (User) session.get(User.class, id);
            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (user != null) {
            return user;
        }
        return null;

    }

    @Override
    public List<User> getUsers() {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<User> users = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User";
            System.out.println("query is : " + query);
            users = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }
        return users;
    }

    @Override
    public void deleteUser(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(User.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        User user = null;
        try {

            //start the transaction
            session.beginTransaction();
            //get the user object
            user = (User) session.get(User.class, id);

            //delete the user
            session.delete(user);

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

    }
}
