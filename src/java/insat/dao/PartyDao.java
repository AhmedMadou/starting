/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

import insat.models.Login;
import insat.models.Party;
import java.util.List;

/**
 *
 * @author asus
 */
public interface PartyDao {

    public void register(Party party);

    public Party validateParty(Login login);

    public List<Party> partySearch(String name);

    public Party getParty(long id);

    public List<Party> getPartys();

    public void deleteParty(long id);
    
    public void updateParty(Party party);
}
