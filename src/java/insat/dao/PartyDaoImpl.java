/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

import insat.models.Login;
import insat.models.Party;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author Madou
 */
public class PartyDaoImpl implements PartyDao {

    public void register(Party party) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();

        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            session.save(party);
            //commit the transaction
            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

    public Party validateParty(Login login) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Party> party;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from Party as p where p.party_login='" + login.getLogin() + "' and u.party_password='" + login.getPassword() + "'";
            System.out.println("query is : " + query);
            party = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (!party.isEmpty()) {
            return party.get(0);
        }
        return null;

    }

    public List<Party> partySearch(String name) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Party> party;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from Party as p where lower(p.party_nom) like lower('%" + name + "%')";
            System.out.println("query is : " + query);
            party = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        return party;
    }

    @Override
    public Party getParty(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        Party party = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            /*String query = "from User as u where u.user_ID=" + id;
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();*/

            party = (Party) session.get(Party.class, id);
            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (party != null) {
            return party;
        }
        return null;

    }

    @Override
    public List<Party> getPartys() {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Party> partys = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from Party";
            System.out.println("query is : " + query);
            partys = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }
        return partys;
    }

    @Override
    public void deleteParty(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Party.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        Party party = null;
        try {

            //start the transaction
            session.beginTransaction();
            //get the user object
            party = (Party) session.get(Party.class, id);

            //delete the user
            session.delete(party);

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

    }
    


    public void updateParty(Party party) {
        Configuration con= new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Party.class);
		
	SessionFactory sf= con.buildSessionFactory();
	Session session=sf.openSession();
	Transaction tx= session.beginTransaction();
        session.update(party);
        tx.commit();
    }
    
}
