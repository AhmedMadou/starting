/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

import insat.models.Municipalite;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author asus
 */
public class MunicipaliteDao {
    
    public List<Municipalite> listeMunicipaliteParEtat(String etat)
    {
    Configuration con= new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Municipalite.class);
		
	SessionFactory sf= con.buildSessionFactory();
	Session session=sf.openSession();
	Transaction tx= session.beginTransaction();
        List<Municipalite> m =(List<Municipalite>) session.createCriteria(Municipalite.class).list();
        List<Municipalite> j=new ArrayList<>();
        for (int i=0;i<m.size();i++)
        {
            if (m.get(i).getMunicipalite_etat().equals(etat))
                j.add(m.get(i));
    
        }
		
	tx.commit();
        return j;
    }
     
    
}
