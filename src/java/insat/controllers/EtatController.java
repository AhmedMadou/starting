/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Etat;
import insat.models.Municipalite;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author asus
 */
@Controller
public class EtatController {
    
    @RequestMapping("/AffichageEtat")
    public String getAllEtats(Model model)
    {
        Etat etat=new Etat();
        model.addAttribute("listeEtat",etat.listeDesEtats());
        return "AffichageEtat";
    }
    
     @RequestMapping( value="EtatController",method = RequestMethod.GET)
    public String getAllSpecificJouet(Model model,HttpServletRequest request)
    {
        
        Municipalite municipalite=new Municipalite();
        model.addAttribute("listeMunicipalite", municipalite.listeMunicipaliteParEtat(request.getParameter("link_codeEtat")));
        model.addAttribute("nomEtat",request.getParameter("link_codeEtat"));
        return "listeMunicipalite";
    }
}
