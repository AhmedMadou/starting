/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Login;
import insat.models.Party;
import insat.models.User;
import insat.services.PartyService;
import insat.services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Madou
 */
@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    PartyService partyService;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView mavWelcome = new ModelAndView("userWelcomePage");

        HttpSession session_Project = request.getSession(false);
        if ((null == session_Project) || (null == session_Project.getAttribute("Session_user"))) {
            ModelAndView mav = new ModelAndView("login");
            mav.addObject("login", new Login());
            return mav;
        }

        User user = new User();
        user = (User) session_Project.getAttribute("Session_user");
        mavWelcome.addObject("user", user);
        return mavWelcome;

    }

    @RequestMapping(value = "/userLoginProcess", method = RequestMethod.POST)
    public ModelAndView userLoginProcess(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("login") Login login) {

        ModelAndView mav = null;
        User user = userService.validateUser(login);

        if (null != user) {
            mav = new ModelAndView("redirect:/welcome");

            HttpSession session_SocialMedia = request.getSession();
            if (null == session_SocialMedia.getAttribute("Session_user")) {
                session_SocialMedia.setAttribute("Session_user", user);
            }
        } else {
            mav = new ModelAndView("login");
            mav.addObject("message", "Username or Password is wrong !");
        }
        return mav;
    }

    @RequestMapping(value = "/partyLoginProcess", method = RequestMethod.POST)
    public ModelAndView partyLoginProcess(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("login") Login login) {

        ModelAndView mav = null;
        Party party = partyService.validateParty(login);

        if (null != party) {
            mav = new ModelAndView("redirect:/welcomeParty");

            HttpSession session_SocialMedia = request.getSession();
            if (null == session_SocialMedia.getAttribute("Session_party")) {
                session_SocialMedia.setAttribute("Session_party", party);
            }
        } else {
            mav = new ModelAndView("login");
            mav.addObject("message", "Username or Password is wrong !");
        }
        return mav;
    }
}
