/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Etat;
import insat.models.Party;
import insat.services.PartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author asus
 */
@Controller
public class VoteController {
    
    @Autowired
    PartyService partyService;
    
     @RequestMapping( value= "/AffichageVote" )
    public String AffichageVote(Model model)
    {
        Party party=new Party();
        model.addAttribute("party", party);
        return "AffichageVote";
    }
    
    @RequestMapping(value="/vote",method = RequestMethod.POST)
    public String vote(@ModelAttribute("party") Party form, Model modelT)
    {
        Party result =partyService.getParty(form.getParty_login());
        int r=result.getParty_nvote();
        result.setParty_nvote(r+1);
        return "ok";
    }
    
}
