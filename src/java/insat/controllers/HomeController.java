/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Admin;
import insat.models.Login;
import insat.models.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Madou
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("index");
        return mav;

    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView getWelcomePageForUser(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("userWelcomePage");

        HttpSession session_Project = request.getSession(false);
        if (null == session_Project.getAttribute("Session_user")) {
            return new ModelAndView("redirect:/login");
        }

        User user = new User();
        user = (User) session_Project.getAttribute("Session_user");
        mav.addObject("user", user);

        return mav;

    }

    @RequestMapping(value = "/welcomeAdmin", method = RequestMethod.GET)
    public ModelAndView getWelcomePageForAdmin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("adminWelcomePage");

        HttpSession session_Project = request.getSession(false);
        if (null == session_Project.getAttribute("Session_admin")) {
            return new ModelAndView("redirect:/login");
        }

        Admin admin = new Admin();
        admin = (Admin) session_Project.getAttribute("Session_admin");
        mav.addObject("admin", admin);

        return mav;

    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session_SocialMedia = request.getSession(false);
        session_SocialMedia.invalidate();

        return new ModelAndView("redirect:/login");
    }

}
