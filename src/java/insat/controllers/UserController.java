/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.User;
import insat.services.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Madou
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/searchUser", method = RequestMethod.GET)
    public ModelAndView SearchUser(
            HttpServletRequest request,
            HttpServletResponse response) {

        String searchValue = request.getParameter("name");
        System.out.println(searchValue);
        List<User> users = userService.userSearch(searchValue);

        ModelAndView mav = new ModelAndView("userSearchList");
        mav.addObject("users", users);
        return mav;
    }
}
