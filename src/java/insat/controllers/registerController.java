/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.User;
import insat.services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Madou
 */
@Controller
public class registerController {

    @Autowired
    public UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user", new User());
        return mav;
    }

    @RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
    public ModelAndView addUser(HttpServletRequest request, HttpSession session,
            @ModelAttribute("user") User user) {

        userService.register(user);
        //adding the user to the Session
        HttpSession session_SocialMedia = request.getSession();
        if (null == session_SocialMedia.getAttribute("Session_user")) {
            session_SocialMedia.setAttribute("Session_user", user);
        }
        //preparing welcome View
        ModelAndView mav = new ModelAndView("redirect:/welcome", "Identifier", user.getUser_cin());
        mav.addObject("firstname", user.getUser_nom());
        return mav;
    }
}
